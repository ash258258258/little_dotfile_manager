use std::{fs, io::ErrorKind, path::Path};


#[derive(Subcommand)]
enum Commands {
    /// start ldm
    Start(Start),
    /// alias for `start`
    S(Start),
    /// link the specific dir\file
    Link(Link),
    /// alias for `link`
    L(Link),
    /// add link
    Add(Add),
    /// alias for `Add`
    A(Add),
}

fn main() {
    let ldm_config = get_conf();
    if let Err(err) = ldm_config {
        eprintln!("{err}");
        return;
    }
    let ldm_config = ldm_config.unwrap();

    let cli = Cli::parse();

    let add = if let Commands::A(args) | Commands::Add(args) = &cli.command {
        args.path.as_str()
    } else {
        ""
    };
    if !add.is_empty() {
        let link_path = fs::read_link(add);
        if link_path.is_err() {
            let err_kind = link_path.err().unwrap().kind();
            match err_kind {
                ErrorKind::NotFound => eprintln!("File or directory not exist"),
                ErrorKind::InvalidInput => eprintln!("This not a link"),
                _ => eprintln!("Something wrong"),
            }
            return;
        }
        let mut conf = ldm_config;
        let modified_conf = conf.get_mut("link").unwrap().as_mapping_mut().unwrap();
        modified_conf.insert(Value::String(absolute_path(add)), {
            let mut new_mapping = Mapping::new();
            let link_path = absolute_path(link_path.as_ref().unwrap().to_str().unwrap());
            new_mapping.insert(Value::String("path".to_owned()), Value::String(link_path));
            serde_yaml::Value::Mapping(new_mapping)
        });
        let config_path = if let Commands::S(args) | Commands::Start(args) = &cli.command {
            &args.path
        } else {
            "ldm.yaml"
        };
        let file = fs::File::create(config_path).unwrap();
        serde_yaml::to_writer(file, &conf).unwrap();
        let hint = format!(
            "add {:?} into configuration successfully",
            link_path.unwrap()
        )
        .green();
        println!("{hint}");
        return;
    }
}
